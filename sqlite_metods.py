import sqlite3


def connect_data():
    # Criando uma conexão com o banco
    conn = sqlite3.connect('mueus_dados.db')
    return conn

# Métodos para criar mais de uma tabela
def create_tables():
    conn = connect_data()
    cursor = conn.cursor()
    # Criando as tabelas
    cursor.executescript('''
        CREATE TABLE IF NOT EXISTS clientes (
            cliente_id INTEGER PRIMARY KEY AUTOINCREMENT,
            primeiro_nome TEXT NOT NULL,
            sobrenome TEXT NOT NULL,
            idade INTEGER NOT NULL,
            cidade TEXT NOT NULL 
        );
        
        CREATE TABLE IF NOT EXISTS pedidos(
            cliente_id INTEGER NOT NULL,
            pedido_id INTEGER PRIMARY KEY AUTOINCREMENT,
            descricao TEXT NOT NULL,
            preco REAL NOT NULL,
            data TEXT NOT NULL,
            FOREIGN KEY (cliente_id)
                REFERENCES clientes (cliente_id)
        );
    ''')
    # Persistindo os dados
    conn.commit()
    # encerrando a conexão
    conn.close()


# Inserindo clientes
def insert_clientes(clientes):
    conn = connect_data()
    cursor = conn.cursor()
    cursor.executemany('''
        INSERT INTO clientes (primeiro_nome, sobrenome, idade, cidade)
        VALUES (?, ?, ?, ?);
    ''', clientes)
    conn.commit()
    conn.close()


# Inserindo pedidos
def insert_pedidos(pedidos):
    conn = connect_data()
    cursor = conn.cursor()
    cursor.executemany('''
        INSERT INTO pedidos (cliente_id, descricao, preco, data)
        VALUES (?, ?, ?, ?);
    ''', pedidos)
    conn.commit()
    conn.close()


# Lendo todos os clientes
def get_all_clientes():
    clientes = []
    try:
        conn = connect_data()
        # Acessando baseado em nome das colunas
        conn.row_factory = sqlite3.Row
        cursor = conn.cursor()
        cursor.execute('''
            SELECT * FROM clientes;
        ''')
        # obtendo as linhas
        rows = cursor.fetchall()

        # Convertendo as linhas em um dicionário
        for row in rows:
            cliente = {
                'cliente_id': row['cliente_id'],
                'primeiro_nome': row['primeiro_nome'],
                'sobrenome': row['sobrenome'],
                'idade': row['idade'],
                'cidade': row['cidade']
            }
            clientes.append(cliente)
    except:
        clientes = []

    return clientes


if __name__ == '__main__':
    for cliente in get_all_clientes():
        print(cliente)

    # create_tables()
    # clientes = [
    #     ("João", "da Silva", 49, "Rio de Janeiro"),
    #     ("Júlia", "Silva e Silva", 59, "São Paulo"),
    #     ("Bertina", "Almeida", 50, "João Pessoa"),
    #     ("Jefferson", "Oliveira", 80, "Recife"),
    #     ("Milena", "Bonaparte", 30, "Salvador"),
    #     ("Maria", "Marieta da Luz", 37, "Natal"),
    # ]
    # pedidos = [
    #     (3, "Salada com fritas", 15, "2022-02-13"),
    #     (2, "Suco, Refrigerante, arroz e feijão", 45.6, "2022-04-04"),
    #     (3, "Arroz com bife", 25.6, "2022-04-14"),
    #     (5, "Café, pão e queijo", 9.5, "2022-03-14"),
    #     (5, "Vinho, arroz e peixe", 39.5, "2022-04-11"),
    #     (2, "Salada de frutas e ovos", 19.8, "2022-04-15"),
    #     (4, "Pizza de atum e refrigerante", 49.8, "2022-04-10"),
    #     (1, "Macarronada, lasanha, arroz com salmão", 109.8, "2022-04-12")
    # ]

    # insert_clientes(clientes)
    # insert_pedidos(pedidos)

